var gulp = require('gulp');

var browserSync = require('browser-sync');
var nunjucksRender = require('gulp-nunjucks-render');
var prettify = require('gulp-html-prettify');
var inlinesource = require('gulp-inline-source')
var htmlmin = require('gulp-htmlmin');
var mjml = require('gulp-mjml');
var pug = require('gulp-pug');
var rename = require('gulp-rename');
var util = require('gulp-util');
var plumber = require('gulp-plumber');
var errorHandler = require('./error-handler');

gulp.task('email', function() {
  gulp.src('./source/views/templates/*.twig')
    .pipe(plumber({errorHandler: errorHandler('Error in [email] task')}))
    .pipe(nunjucksRender({
      path: ['./source/views/']
    }))
    .pipe(prettify({indent_char: '\t', indent_size: 1}))
    .pipe(rename({
      extname: '.mjml'
    }))
    .pipe(inlinesource())
    .pipe(mjml())
    // .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./build'));
});

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: "./build"
    },
    startPath: ''
  });
});

gulp.task('watch', function(){
  gulp.watch('./source/views/**/*.twig', ['email']).on('change', browserSync.reload);
});

gulp.task('default', ['email', 'watch', 'browser-sync']);
